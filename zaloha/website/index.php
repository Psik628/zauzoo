<?php
	if (!isset($lang)) $lang = 'cs';
	$ini_array = parse_ini_file("../i18n/".$lang.".ini");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<html>
    <head>
        <title>MultiData Bohemia</title>

	<meta name="keywords" content="programování,vývoj software,software,informační systémy,ERP,webové stránky,správa HW,správa sítí">
	<meta name="description" content="Již 13 let děláme, co nás baví. Vyvíjíme a implementujeme našim zákazníkům software na míru a to musí být někde znát… Naši klienti již mají náskok před konkurencí. Zařaďte se mezi ně!">
	<meta name="author" content="MultiData Bohemia spol s r.o.">

	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"> 
	<meta http-equiv="cache-control" content="max-age=0" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="expires" content="0" />
	<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
	<meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="Content-language" content="cs">

	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="../css/style.css?2">
	<link rel="shortcut icon" href="../favicon.ico" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-55237331-1', 'auto');
	  ga('send', 'pageview');

	</script>
	<script>
            var presentations = ["expobank-internetbanking", "editel", "expobank", "pasy", "fuelcheck", "schiedel", "md"];
	    var current = 0;
	    var intervalFunction = null;
	    var projectInfoShown = false;
	    
            $(function() {
	        $("#bang").load("../website/presentation/text/"+presentations[0]+".php?lang=<?php echo $lang; ?>&"+(new Date().getTime()));
    	        $("#frame").attr("src", "../website/presentation/image/"+presentations[0]+".png");
		checkEditel();
		$("#pointers").empty();
		for (i = 0; i< presentations.length; i++) {
		    if (i === current) {
		        $("#pointers").append("<div onclick='showPresentation("+i+");' class='pointer pointer-selected'></div>");
		    } else {
		        $("#pointers").append("<div onclick='showPresentation("+i+");' class='pointer'></div>");
		    }
		}
	    });
            $(function() {
		intervalFunction = setInterval(function() {
		    current = (current+1)%presentations.length;
		    $("#bang").css("display", "none");
		    $("#bang").load("../website/presentation/text/"+presentations[current]+".php?lang=<?php echo $lang; ?>&"+(new Date().getTime()));
		    $("#bang").fadeIn(1000);
		    $("#frame").attr("src", "../website/presentation/image/"+presentations[current]+".png");

		    checkEditel();

		    $("#pointers").empty();
		    for (i = 0; i< presentations.length; i++) {
		        if (i === current) {
		            $("#pointers").append("<div onclick='showPresentation("+i+");' class='pointer pointer-selected'></div>");
		        } else {
		            $("#pointers").append("<div onclick='showPresentation("+i+");' class='pointer'></div>");
		        }
		    }
		}, 10000);
	    });

	    function showPresentation(number) {
		current = number;
//	        $("#bang").css("display", "none");
		$("#bang").load("../website/presentation/text/"+presentations[current]+".php?lang=<?php echo $lang; ?>&"+(new Date().getTime()));
//		$("#bang").fadeIn(1000);
		$("#frame").attr("src", "../website/presentation/image/"+presentations[current]+".png");
		
		checkEditel();	

		$("#pointers").empty();
		for (i = 0; i< presentations.length; i++) {
		    if (i === current) {
		        $("#pointers").append("<div onclick='showPresentation("+i+");' class='pointer pointer-selected'></div>");
		    } else {
		        $("#pointers").append("<div onclick='showPresentation("+i+");' class='pointer'></div>");
		    }
		}
		clearInterval(intervalFunction);
		if (projectInfoShown) {
		    showProjectInfo(presentations[current], false);
		}
	    };


	    var position = 0;
	    function showInfo(page) {
		if ($("#"+page).css("zIndex") === "100") {
		    $("#"+page+" .button").css("visibility", "hidden");
		    $("#"+page).animate({position: "absolute", width: "232px", left: position},500, function() {$("#"+page+" .button").css("visibility", "visible"); $("#"+page).css("zIndex", "1");});
		    $("#"+page+" .button").text("<?php echo $ini_array['www.page.services.moreinfo']; ?>");
		} else {
	            position = $("#"+page).css("left");
		    $("#"+page+" .button").css("visibility", "hidden");
		    $("#"+page).css("backgroundColor", "white");
		    $("#"+page).css("zIndex", "100");
		    $("#"+page).animate({position: "absolute", width: "992px", left: "0px"},500, function() {$("#"+page+" .button").css("visibility", "visible");});
		    $("#"+page+" .button").text("<?php echo $ini_array['www.page.services.lessinfo']; ?>");
		}
	    };

	    function showReferences() {
		if ($("#references").css("height") === "326px") {
		    $("#references").animate({height: "466px"},500);
		} else if ($("#references").css("height") === "466px") {
		    $("#references").animate({height: "326px"},500);
		}
	    }
	    
 	    function showProjectInfo(project, scroll) {
		projectInfoShown = true;
		clearInterval(intervalFunction);
		$( ".project" ).css( "height", "0px" );
		$('#project-'+project).css( "height", "auto" );
		if (scroll) {
		    $("html, body").animate({ scrollTop: $('#project-'+project).offset().top - 95 }, 500);
		}

		/*$( ".project" ).css( "height", "0px" );
		window.location.hash = '#project-'+project;
		var el = $('#project-'+project);
    		var autoHeight = el.css('height', 'auto').height();
		el.height("0px").animate({height: autoHeight}, 500);*/
	    }

	    function hideProjectInfo(project) {
		projectInfoShown = false;
/*		$( ".project" ).animate({height: "0px"}, 500);*/
		var body = $("html, body");
		body.animate({scrollTop:0}, '500', 'swing', function() {$( ".project" ).css( "height", "0px" );});
	    }

	    function rollToElement(elementId) {
		$('html, body').animate({
        		scrollTop: $(elementId).offset().top - 95
		}, 300);
	    }

	    function checkEditel() {
		
		if (presentations[current] == "editel") {
			$('div.macbook').append('<img src="../website/presentation/image/computerworld.png" class="price">');
			$('div.macbook > img.price').css('position', 'absolute');
			$('div.macbook > img.price').css('top', '-66px');
			$('div.macbook > img.price').css('left', '307px');
		} else {
			$('div.macbook > img.price').remove();
		}
	    }

	</script> 
    </head>
    <body>
	<div class="wrapper wrapper-top" style="background-color: black">
            <div class="page top">
		<a href="."><img src="../images/logo.png" class='logo'></img></a>
		<div class='language-menu'>
		    <a href='../en/' title="English" style="font-weight: <?php if ($lang == "en") echo "bold"; else echo "normal"; ?>;">EN</a> | 
		    <a href='../de/' title="Deutch" style="font-weight: <?php if ($lang == "de") echo "bold"; else echo "normal"; ?>;">DE</a> | 
		    <a href='../cs/' title="Čeština" style="font-weight: <?php if ($lang == "cs") echo "bold"; else echo "normal"; ?>;">CZ</a>
		</div>
		<ul class='top-menu'>
		    <li onclick="rollToElement('#home');"><?php echo $ini_array['www.page.top.home']; ?></li>
		    <li onclick="rollToElement('#about');"><?php echo $ini_array['www.page.top.about']; ?></li>
		    <li onclick="rollToElement('#services');"><?php echo $ini_array['www.page.top.services']; ?></li>
		    <li onclick="rollToElement('#references');"><?php echo $ini_array['www.page.top.references']; ?></li>
		    <li onclick="rollToElement('#contact');"><?php echo $ini_array['www.page.top.contact']; ?></li>
		</ul>
	    </div>
	</div>
	<div style="position: relative; height: 95px; width: 100%;"></div>
	<div class="wrapper wrapper-head">
	<div class="page" id="home">
	    <div class="macbook">
		<img src="../images/macbook.png" class='macbook'></img>
		<img src="" id="frame" class="frame"></img>
	    </div>
	    <div class="page head">
		<div id="bang" class="bang">
		</div>
		<div id="pointers" class="pointers"></div>
	    </div>
	    <div class="page grey" id="about">
		<div class="text">
		    <span class="name"><?php echo $ini_array['www.page.head.about.title']; ?></span>
		    <span class="description"><?php echo $ini_array['www.page.head.about.desc']; ?></span>
		</div>
	    </div>
	</div>
	</div>
	<div class="wrapper wrapper-project">
		<div class="page project project-editel" id="project-editel">
		<div class="free"></div>
		<span class="name"><?php echo $ini_array['www.page.head.editel.title']; ?></span>
		<div class="description">
<a href="../website/presentation/image/itprodukt_2014.pdf" data-lightbox="project-editel"><img src="../website/presentation/image/itprodukt_2014_tn.png" class="right"></img></a>
<p><?php echo $ini_array['www.page.head.editel.article']; ?></p>
		</div>
		<div class="button" onclick="hideProjectInfo('editel');"><?php echo $ini_array['www.page.head.article.hide']; ?></div>
	    </div>
	</div>

	<div class="wrapper wrapper-content">
	    <div class="page content" id="services">
		<div class="free"></div>
		<div class="name"><?php echo $ini_array['www.page.services.title']; ?></div>
		<div class="description"><?php echo $ini_array['www.page.services.subtitle']; ?></div>
		<div class="offers">
		    <div class="offer offer-web" id="offer-web">
			<img src="../images/ico_web.png" class="icon icon-web"></img>
			<span class="name"><?php echo $ini_array['www.page.services.webpages.title']; ?></span>
			<span class="description"><?php echo $ini_array['www.page.services.webpages.desc']; ?></span>
			<div class="button" onclick="showInfo('offer-web');"><?php echo $ini_array['www.page.services.moreinfo']; ?></div>
			<div class="text">
				<?php echo $ini_array['www.page.services.webpages.article']; ?>
			</div>
		    </div>
		    <div class="offer offer-is"  id="offer-is">
			<img src="../images/ico_is.png" class="icon icon-is"></img>
			<span class="name"><?php echo $ini_array['www.page.services.is.title']; ?></span>
			<span class="description"><?php echo $ini_array['www.page.services.is.desc']; ?></span>
			<div class="button" onclick="showInfo('offer-is');"><?php echo $ini_array['www.page.services.moreinfo']; ?></div>
			<div class="text">
				<?php echo $ini_array['www.page.services.is.article']; ?>
			</div>
		    </div>
		    <div class="offer offer-programovani" id="offer-programovani">
			<img src="../images/ico_programovani.png" class="icon icon-programovani"></img>
			<span class="name"><?php echo $ini_array['www.page.services.programming.title']; ?></span>
			<span class="description"><?php echo $ini_array['www.page.services.programming.desc']; ?></span>
			<div class="button" onclick="showInfo('offer-programovani');"><?php echo $ini_array['www.page.services.moreinfo']; ?></div>
			<div class="text">
				<?php echo $ini_array['www.page.services.programming.article']; ?>
			</div>
		    </div>
		    <div class="offer offer-konzultace" id="offer-konzultace">
			<img src="../images/ico_konzultace.png" class="icon icon-konzultace"></img>
			<span class="name"><?php echo $ini_array['www.page.services.networking.title']; ?></span>
			<span class="description"><?php echo $ini_array['www.page.services.networking.desc']; ?></span>
			<div class="button" onclick="showInfo('offer-konzultace');"><?php echo $ini_array['www.page.services.moreinfo']; ?></div>
			<div class="text">
				<?php echo $ini_array['www.page.services.networking.article']; ?>
			</div>
		    </div>
		</div>
	    </div>
	</div>
	<div class="wrapper wrapper-references">
	    <div class="page references" id="references">
		<span class="name"><?php echo $ini_array['www.page.references.title']; ?></span>
		<span class="description"><?php echo $ini_array['www.page.references.subtitle']; ?></span>
		<div class="images">
		    <div>
			<img src="../images/schiedel.png" class="ref-image" style="position: relative;"></img>
			<!--<img src="../images/autoneum.png" class="ref-image" style="position: relative; bottom: 16px;"></img>-->
			<img src="../images/editel.png" class="ref-image" style="position: relative; bottom: 10px;"></img>
			<img src="../images/cs.png" class="ref-image" style="position: relative;"></img>
		    </div>
		</div>
		<!--div class="button-wrapper">
		    <div class="button" onclick="showReferences()"><?php echo $ini_array['www.page.references.another']; ?></div4>
		</div-->
	    </div>
	<div class="wrapper wrapper-products">
            <div class="page products" id="contact">
		<!--span class="name1">Produkty</span>
		<span class="description1"><span class="item">Položka1</span> | <span class="item">Položka2</span> | <span class="item">Položka3</span> | <span class="item">Položka4</span></span-->
		<span class="name2"><?php echo $ini_array['www.page.footer.title']; ?></span>
		<span class="description2">MultiData Bohemia spol. s r.o.<br />Nad Kajetánkou 1783/13, 169 00 Praha 6<br /><?php echo $ini_array['www.page.footer.branch']; ?>: JUDr. Krpaty 1147, 530 03 Pardubice<br />mail: <a href="mailto:info@multi-data.cz">info@multi-data.cz</a>, tel.: +420 777 481 622<br /><?php echo $ini_array['www.page.footer.companynr']; ?>: 26710579</span>
            </div>
	</div>
	<div class="wrapper wrapper-footer">
	    <div class="page footer">
		&copy; All rights reserved 2018. MultiData Bohemia spol. s r.o.
	    </div>
	</div>
    </body>
</html>
