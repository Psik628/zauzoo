<?php
    if (isset($_GET['lang'])) {
	$ini_array = parse_ini_file("../../../i18n/".$_GET['lang'].".ini");
    } else {
	$ini_array = parse_ini_file("../../../i18n/cs.ini");
    }
?>
<span class="name"><?php echo $ini_array['www.page.head.editel.title']; ?></span>
<span class="description"><?php echo $ini_array['www.page.head.editel.desc']; ?></span>
<div class="buttons"><div class="button" onclick="showProjectInfo('editel', true);"><?php echo $ini_array['www.page.head.moreinfo']; ?></div></div>
